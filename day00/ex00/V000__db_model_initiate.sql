create table indicator
(
  id			bigint				NOT NULL
  ,name			varchar				NOT NULL
  ,unit			varchar				NOT NULL
  ,CONSTRAINT	pk_indicator		PRIMARY KEY (id)
  ,CONSTRAINT	uk_indicator_name	UNIQUE (name)
);
comment on table indicator			is	'Historical values for each country and period of time';
comment on column indicator.id		is	'Indicator by order';
comment on column indicator.name	is	'Indicator name';
comment on column indicator.unit	is	'Indicator unit';
create table country
(
  id			bigint						NOT NULL
  ,name			varchar 					NOT NULL
  ,par_id		bigint
  ,object_type	varchar						NOT NULL					DEFAULT 'country'
  ,CONSTRAINT	pk_country					PRIMARY KEY (id)
  ,CONSTRAINT	uk_country_name_object_type	UNIQUE (name, object_type)
  ,CONSTRAINT	fk_par_id_country 			FOREIGN KEY (par_id)		REFERENCES country(id)
);
comment on table country				is	'Continent/Country of The World';
comment on column country.id			is	'Continent/Country by order';
comment on column country.name			is	'Continent/Country name';
comment on column country.par_id		is	'Continent id for country, null for continent';
comment on column country.object_type	is	'Continent or Country';

create table country_indicator
(
  id			bigint										NOT NULL
  ,c_id			bigint 										NOT NULL
  ,i_id			bigint 										NOT NULL
  ,value		numeric
  ,actual_date	timestamp 									NOT NULL
  ,CONSTRAINT	pk_country_indicator						PRIMARY KEY (id)
  ,CONSTRAINT	uk_country_indicator_c_id_i_id_actual_date	UNIQUE (c_id,i_id,actual_date)
  ,CONSTRAINT	fk_c_id_country								FOREIGN KEY (c_id)	REFERENCES country(id)
  ,CONSTRAINT	fk_i_id_indicator							FOREIGN KEY (i_id)	REFERENCES indicator(id)
);
comment on table country_indicator				is 'Country to indicator';
comment on column country_indicator.id			is 'Country to indicator id';
comment on column country_indicator.c_id		is 'Country id';
comment on column country_indicator.i_id		is 'Indicator id';
comment on column country_indicator.value		is 'Value';
comment on column country_indicator.actual_date	is 'Actual date';